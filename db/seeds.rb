# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
20.times{|i|
  item = Item.create(title: "Rubyの勉強その:#{i}", at: Date.today, spent: 1, note: "特になし" )  
}

#  id         :integer          not null, primary key
#  title      :string(255)
#  at         :date
#  spent      :float
#  note       :text
#  created_at :datetime
#  updated_at :datetime