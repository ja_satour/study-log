class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name
      t.boolean :is_deleted

      t.timestamps
    end
  end
end
