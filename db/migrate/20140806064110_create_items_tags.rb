class CreateItemsTags < ActiveRecord::Migration
  def change
    create_table :items_tags do |t|
      t.references :item, index: true
      t.references :tag, index: true

      t.timestamps
    end
  end
end
