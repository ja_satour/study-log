class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :title
      t.date :at
      t.float :spent
      t.text :note

      t.timestamps
    end
  end
end
