source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

gem 'devise'
gem 'cancancan'
gem 'rails-erd'
gem 'kaminari'
gem 'carrierwave'
gem 'active_decorator'

group :development, :test do
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'

  #testing tool
  gem 'minitest-rails'

  #misc
  gem 'annotate', ">=2.6.0" #append database schema info to Model file.
  gem 'bullet' #detect N+1

  #for better repl
  gem 'pry'
  gem 'pry-doc'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'hirb' #make sure that $Hirb.enable is necessary when start using Hirb, also $Hirb.disabled when stop using it
  gem 'hirb-unicode'
  
  #for better error
  gem 'better_errors', '~> 1.1.0'
  gem 'binding_of_caller'
end

group :production do
  gem 'pg'
  gem 'rails_12factor'
end