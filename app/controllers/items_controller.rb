class ItemsController < ApplicationController

  ITEMS_PER_PAGE = 10
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  before_action :set_items_per_page, only: [:index]
  before_action :get_tags, only: [:new, :edit]
  
  # GET /items
  # GET /items.json
  def index
    @items_all = Item.all.order(at: :desc)
    @items = @items_all.page(params[:page]).per(ITEMS_PER_PAGE)
    request_putter
  end

  # GET /items/1
  # GET /items/1.json
  def show
  end

  # GET /items/new
  def new
    @item = Item.new
  end

  # GET /items/1/edit
  def edit
  end

  # POST /items
  # POST /items.json
  def create
    begin
      @item = Item.new(item_params)
        respond_to do |format|
          if @item.save
            format.html { redirect_to items_url, notice: t("create.success") }
          else
            format.html { render :new }
            format.json { render json: @item.errors, status: :unprocessable_entity }
          end
      end
    rescue
      render :new, notice: 'Something was wrong.'
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    begin
        respond_to do |format|
          if @item.update(item_params)
            format.html { redirect_to items_url, notice: t("update.success") }
            format.json { render :show, status: :ok, location: @item }
          else
            format.html { render :edit }
            format.json { render json: @item.errors, status: :unprocessable_entity }
          end
        end
    rescue
        render :edit, notice: 'Something was wrong.'
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: t("destroy.success") }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:title, :at, :spent, :note)
    end

    def set_items_per_page
      @items_per_page = ITEMS_PER_PAGE
    end

    def request_putter
      i = 1
      puts "------------------------(；´Д`)HTTPリクエストです"
      request.env.each_pair{|key, value| puts "#{i}--#{key}:#{value}";i+=1;}
      puts "------------------------(；´Д`)以上です"
    end

    def get_tags
      @tags = Tag.order(created_at: :desc)
    end

end
