# == Schema Information
#
# Table name: items
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  at         :date
#  spent      :float
#  note       :text
#  created_at :datetime
#  updated_at :datetime
#

class Item < ActiveRecord::Base

  has_and_belongs_to_many :tags

  validates :title,
    presence: true,
    length: { maximum: 50 }

  validates :spent,
    presence: true,
    numericality: true

  validates :note,
    length: { maximum: 1000 }

end
