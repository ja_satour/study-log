json.array!(@tags) do |tag|
  json.extract! tag, :id, :name, :is_deleted
  json.url tag_url(tag, format: :json)
end
