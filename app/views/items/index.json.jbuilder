json.array!(@items) do |item|
  json.extract! item, :id, :title, :at, :spent, :note
  json.url item_url(item, format: :json)
end
