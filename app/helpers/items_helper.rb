module ItemsHelper
  
  #TODO! 18nできないか？ WDAYS = ["日","月","火","水","木","金","土"]
  WDAYS = ["(sun)","(mon)","(tue)","(wed)","(thr)","(fri)","(sat)"]
  
  def link_to_new
    link_to t("label.create"), new_item_path, :class => "navigate-right"
  end
  
  def link_to_tag_index
    link_to t("label.tags"), tags_url, :class => "navigate-right"
  end
  
  def getToday
    Time.zone.now.strftime("%Y-%m-%d") 
  end
  
  def getYoubi(day)
    return WDAYS[day]
  end
  
  def getTodayYoubi
    day = Time.now.wday
    return WDAYS[day]
  end
end
