require "test_helper"

class ItemsControllerTest < ActionController::TestCase

  def item
    @item ||= items :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:items)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference('Item.count') do
      post :create, item: { at: @item.at, note: @item.note, spent: @item.spent, title: @item.title }
    end

    assert_redirected_to item_path(assigns(:item))
  end

  def test_show
    get :show, id: item
    assert_response :success
  end

  def test_edit
    get :edit, id: item
    assert_response :success
  end

  def test_update
    put :update, id: item, item: { at: @item.at, note: @item.note, spent: @item.spent, title: @item.title }
    assert_redirected_to item_path(assigns(:item))
  end

  def test_destroy
    assert_difference('Item.count', -1) do
      delete :destroy, id: item
    end

    assert_redirected_to items_path
  end
end
