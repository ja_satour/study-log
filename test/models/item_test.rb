# == Schema Information
#
# Table name: items
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  at         :date
#  spent      :float
#  note       :text
#  created_at :datetime
#  updated_at :datetime
#

require "test_helper"

class ItemTest < ActiveSupport::TestCase

  def item
    @item ||= Item.new
  end

  def test_valid
    assert item.valid?
  end

end
