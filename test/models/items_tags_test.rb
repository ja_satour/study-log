require "test_helper"

class ItemsTagsTest < ActiveSupport::TestCase

  def items_tags
    @items_tags ||= ItemsTags.new
  end

  def test_valid
    assert items_tags.valid?
  end

end
